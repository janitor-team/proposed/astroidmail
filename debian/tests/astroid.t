use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my @CMD = ( 'xvfb-run', '-a', 'astroid' );

run_ok @CMD, qw(--help);
like stdout, qr/welcome to astroid!/, 'help, stdout';
#cmp_ok stderr, 'eq', '', 'help, stderr';

run_ok @CMD, qw(--foo);
like stdout, qr/unrecognised option/, 'foo, stdout';
#cmp_ok stderr, 'eq', '', 'foo, stderr';

done_testing;
